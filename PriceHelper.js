const fetch = require('node-fetch');

module.exports = class PriceHelper {
  static getBitcoinPrice() {
    var baseUrl = "https://blockchain.info";
    var dayPriceUrl = baseUrl + "/q/24hrprice";
    return new Promise((resolve, reject) => {
      fetch(dayPriceUrl).then((response) => {
        response.json().then((json) => {
          resolve(json);
        }, (e) => {
          reject(e);
        });
      }, (e) => {
        reject(e);
      });
    });
  }

  static satoshiToUSD(satoshis) {
    return new Promise((resolve, reject) => {
      this.getBitcoinPrice().then((price) => {
        let btc = satoshis/100000000;
        let usd = parseFloat((price*btc).toFixed(2));
        resolve(usd);
      }, (e) => {
        reject(e);
      });
    });
  }
}
